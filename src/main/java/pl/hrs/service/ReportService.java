package pl.hrs.service;

import pl.hrs.model.Hotel;
import pl.hrs.model.ReservationRequest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class ReportService {
    //Total value of bookings, for each arrival date
    public Map<LocalDate, BigDecimal> getTotalValueOfBookings(List<ReservationRequest> reservations) {
        return reservations.stream()
                .filter(reservation -> ReservationRequest.ReservationOutcome.ACCEPTED.equals(reservation.getReservationOutcome()))
                .collect(groupingBy(ReservationRequest::getArrival,
                        Collectors.reducing(BigDecimal.ZERO, ReservationRequest::calculateTotalPrice, (a, b) -> a.add(b))));
    }

    //Hotels ranked by total bookings value (sum of values of all bookings for that hotel)
    public Map<Hotel, BigDecimal> getHotelsWithTotalBookingsValue(List<ReservationRequest> reservations) {
        return reservations.stream()
                .filter(reservation -> ReservationRequest.ReservationOutcome.ACCEPTED.equals(reservation.getReservationOutcome()))
                .collect(groupingBy(ReservationRequest::getHotel,
                        Collectors.reducing(BigDecimal.ZERO, ReservationRequest::calculateTotalPrice, (a, b) -> a.add(b))));
    }

    // Dates, ranked by the highest amount of a single rejection with such arrival date
    public Map<LocalDate, BigDecimal> getDatesRankedByHighestRejection(List<ReservationRequest> reservations) {
        return reservations.stream()
                .filter(reservation -> ReservationRequest.ReservationOutcome.REJECTED.equals(reservation.getReservationOutcome()))
                .collect(groupingBy(ReservationRequest::getArrival,
                        Collectors.reducing(BigDecimal.ZERO, ReservationRequest::calculateTotalPrice, (a, b) -> a.compareTo(b) > 0 ? a : b)));
    }
}
