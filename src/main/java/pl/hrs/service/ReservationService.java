package pl.hrs.service;

import pl.hrs.model.ReservationRequest;

import static pl.hrs.util.Utils.iterateFromTo;

public class ReservationService {
    public ReservationRequest processReservationRequest(ReservationRequest reservationRequest) {
        if (reservationIsPossible(reservationRequest)) {
            makeReservation(reservationRequest);
            reservationRequest.setReservationOutcome(ReservationRequest.ReservationOutcome.ACCEPTED);
        } else {
            reservationRequest.setReservationOutcome(ReservationRequest.ReservationOutcome.REJECTED);
        }

        return reservationRequest;
    }

    private void makeReservation(ReservationRequest reservationRequest) {
        iterateFromTo(reservationRequest.getArrival(), reservationRequest.getDeparture())
                .forEach(date -> reservationRequest.getHotel()
                        .getRoomAvailabilityMap().get(date).decreaseAvailableRooms(reservationRequest.getNoOfRooms()));
    }

    private boolean reservationIsPossible(ReservationRequest reservationRequest) {
        return iterateFromTo(reservationRequest.getArrival(), reservationRequest.getDeparture())
                .allMatch(date -> reservationRequest.getHotel()
                        .getRoomAvailabilityMap().containsKey(date)
                        && reservationRequest.getHotel().getRoomAvailabilityMap().get(date).getRoomsAvailable() >= reservationRequest.getNoOfRooms());
    }
}
