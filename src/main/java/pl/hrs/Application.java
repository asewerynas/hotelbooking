package pl.hrs;

import pl.hrs.model.Hotel;
import pl.hrs.model.RoomAvailability;
import pl.hrs.model.ReservationRequest;
import pl.hrs.service.ReportService;
import pl.hrs.service.ReservationService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static pl.hrs.util.Utils.printReport;

public class Application {
    private static ReservationService reservationService = new ReservationService();
    private static ReportService reportService = new ReportService();

    public static void main(String[] args) {
        //sample data creation
        Hotel besternWarsawHotel = new Hotel("Bestern Hotel Warsaw");
        Hotel carriotBerlinHotel = new Hotel("Carriot Berlin");
        Hotel pheratonLondonHotel = new Hotel("Pheraton London Central");

        besternWarsawHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 1),
                new RoomAvailability(new BigDecimal("25"), 3));
        besternWarsawHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 2),
                new RoomAvailability(new BigDecimal("20.5"), 4));
        carriotBerlinHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 1),
                new RoomAvailability(new BigDecimal("12"), 7));
        carriotBerlinHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 2),
                new RoomAvailability(new BigDecimal("10.25"), 2));
        carriotBerlinHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 3),
                new RoomAvailability(new BigDecimal("100"), 10));
        pheratonLondonHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 3),
                new RoomAvailability(new BigDecimal("54"), 4));
        pheratonLondonHotel.getRoomAvailabilityMap().put(LocalDate.of(2017, 10, 4),
                new RoomAvailability(new BigDecimal("22"), 5));

        List<ReservationRequest> reservationRequests = Arrays.asList(
                new ReservationRequest(besternWarsawHotel, LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 2), 1),
                new ReservationRequest(besternWarsawHotel, LocalDate.of(2017, 10, 2), LocalDate.of(2017, 10, 2), 1),
                new ReservationRequest(carriotBerlinHotel, LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 3), 3),
                new ReservationRequest(carriotBerlinHotel, LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 3), 2),
                new ReservationRequest(pheratonLondonHotel, LocalDate.of(2017, 10, 4), LocalDate.of(2017, 10, 5), 3),
                new ReservationRequest(pheratonLondonHotel, LocalDate.of(2017, 10, 3), LocalDate.of(2017, 10, 4), 3),
                new ReservationRequest(pheratonLondonHotel, LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 4), 8)
        );

        //reservation procedure
        List<ReservationRequest> processedReservations = reservationRequests.stream()
                .map(reservationService::processReservationRequest).collect(Collectors.toList());

        //building reports and printing results
        printReport("Total value of bookings, for each arrival date",
                reportService.getTotalValueOfBookings(processedReservations));
        printReport("Hotels ranked by total bookings value (sum of values of all bookings for that hotel)",
                reportService.getHotelsWithTotalBookingsValue(processedReservations));
        printReport("Dates, ranked by the highest amount of a single rejection with such arrival date",
                reportService.getDatesRankedByHighestRejection(processedReservations));
    }
}
