package pl.hrs.model;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Hotel {
    private String name;
    private Map<LocalDate, RoomAvailability> roomAvailabilityMap = new HashMap<>();

    public Hotel(String name) {
        this.name = name;
    }

    public Map<LocalDate, RoomAvailability> getRoomAvailabilityMap() {
        return roomAvailabilityMap;
    }

    public void setRoomAvailabilityMap(Map<LocalDate, RoomAvailability> roomAvailabilityMap) {
        this.roomAvailabilityMap = roomAvailabilityMap;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
