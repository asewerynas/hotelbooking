package pl.hrs.model;

import java.math.BigDecimal;

public class RoomAvailability {
    private BigDecimal price;
    private Integer roomsAvailable;

    public RoomAvailability() {
    }

    public RoomAvailability(BigDecimal price, Integer roomsAvailable) {
        this.price = price;
        this.roomsAvailable = roomsAvailable;
    }

    public void decreaseAvailableRooms(Integer rooms) {
        this.roomsAvailable -= rooms;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getRoomsAvailable() {
        return roomsAvailable;
    }

    public void setRoomsAvailable(Integer roomsAvailable) {
        this.roomsAvailable = roomsAvailable;
    }
}
