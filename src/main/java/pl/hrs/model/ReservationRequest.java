package pl.hrs.model;

import pl.hrs.model.Hotel;

import java.math.BigDecimal;
import java.time.LocalDate;

import static pl.hrs.util.Utils.iterateFromTo;

public class ReservationRequest {
    private Hotel hotel;
    private LocalDate arrival;
    private LocalDate departure;
    private Integer noOfRooms;
    private ReservationOutcome reservationOutcome;

    public enum ReservationOutcome {
        NEW, ACCEPTED, REJECTED
    }

    public ReservationRequest(Hotel hotel, LocalDate arrival, LocalDate departure, Integer noOfRooms) {
        this.hotel = hotel;
        this.arrival = arrival;
        this.departure = departure;
        this.noOfRooms = noOfRooms;
        this.reservationOutcome = ReservationOutcome.NEW;
    }

    public BigDecimal calculateTotalPrice() {
        return iterateFromTo(arrival, departure)
                .map(date -> hotel.getRoomAvailabilityMap().containsKey(date) ?
                        hotel.getRoomAvailabilityMap().get(date).getPrice().multiply(BigDecimal.valueOf(noOfRooms))
                        : BigDecimal.ZERO)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public LocalDate getArrival() {
        return arrival;
    }

    public void setArrival(LocalDate arrival) {
        this.arrival = arrival;
    }

    public LocalDate getDeparture() {
        return departure;
    }

    public void setDeparture(LocalDate departure) {
        this.departure = departure;
    }

    public Integer getNoOfRooms() {
        return noOfRooms;
    }

    public void setNoOfRooms(Integer noOfRooms) {
        this.noOfRooms = noOfRooms;
    }

    public ReservationOutcome getReservationOutcome() {
        return reservationOutcome;
    }

    public void setReservationOutcome(ReservationOutcome reservationOutcome) {
        this.reservationOutcome = reservationOutcome;
    }
}
