package pl.hrs.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.stream.Stream;

public class Utils {
    private Utils() {
        //should not be instantiated
    }

    public static Stream<LocalDate> iterateFromTo(LocalDate from, LocalDate to) {
        return Stream.iterate(from, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(from, to) + 1);
    }

    public static void printReport(String title, Map<? extends Object, BigDecimal> report) {
        System.out.println("--- " + title + " ---");
        for (Map.Entry<? extends Object, BigDecimal> entry : report.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
        System.out.println();
    }
}
